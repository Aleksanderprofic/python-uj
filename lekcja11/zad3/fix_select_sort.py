# Poprawić wybrany algorytm sortowania, aby przyjmował jako dodatkowy argument funkcję porównującą elementy na liście
# [przykład na wykładzie dla funkcji bubblesort()].
import zad1.sample_data as sd


def cmp(x, y):
    if x > y:
        return 1
    elif x == y:
        return 0
    else:
        return -1


def select_sort(l, left, right, cmp_func=cmp):
    for i in range(left, right):
        k = i
        for j in range(i+1, right+1):
            if cmp_func(l[j], l[k]) < 0:
                k = j

        temp = l[i]
        l[i] = l[k]
        l[k] = temp


if __name__ == "__main__":
    l = sd.random_list_with_repeated_values(10)
    print(f"random_list_with_repeated_values before sorting: {l}")
    select_sort(l, 0, len(l) - 1)
    print(f"random_list_with_repeated_values after sorting: {l}\n")

    l = sd.random_unsorted_list(10)
    print(f"random_unsorted_list before sorting: {l}")
    select_sort(l, 0, len(l) - 1)
    print(f"random_unsorted_list after sorting: {l}\n")

    l = sd.random_almost_sorted_desc_order_list(10)
    print(f"random_almost_sorted_desc_order_list before sorting: {l}")
    select_sort(l, 0, len(l) - 1)
    print(f"random_almost_sorted_desc_order_list after sorting: {l}\n")

    l = sd.random_almost_sorted_list(10)
    print(f"random_almost_sorted_list before sorting: {l}")
    select_sort(l, 0, len(l) - 1)
    print(f"random_almost_sorted_list after sorting: {l}\n")

    l = sd.random_gaussian_distribution_numbers_list(10)
    print(f"random_gaussian_distribution_numbers_list before sorting: {l}")
    select_sort(l, 0, len(l) - 1)
    print(f"random_gaussian_distribution_numbers_list after sorting: {l}\n")
