# Przygotować moduł Pythona z funkcjami tworzącymi listy liczb całkowitych do sortowania.

import random
import numpy as np


# (a) różne liczby int od 0 do N-1 w kolejności losowej
def random_unsorted_list(n):
    numbers = []
    for i in range(n):
        numbers.append(i)

    l = []
    numbers_length = n
    while numbers_length > 0:
        index = random.randint(0, numbers_length - 1)
        number = numbers[index]
        del numbers[index]
        l.append(number)

        numbers_length -= 1

    return l


# (b) różne liczby int od 0 do N-1 prawie posortowane (liczby są blisko swojej prawidłowej pozycji)
def random_almost_sorted_list(n):
    l = []
    for i in range(n):
        l.append(i)

    index1 = random.randint(0, n - 1)
    index2 = random.randint(0, n - 1)

    while index1 == index2:
        index2 = random.randint(0, n - 1)

    temp = l[index1]
    l[index1] = l[index2]
    l[index2] = temp

    return l


# (c) różne liczby int od 0 do N-1 prawie posortowane w odwrotnej kolejności,
def random_almost_sorted_desc_order_list(n):
    l = []
    i = n - 1
    while i >= 0:
        l.append(i)
        i -= 1

    index1 = random.randint(0, n - 1)
    index2 = random.randint(0, n - 1)

    while index1 == index2:
        index2 = random.randint(0, n - 1)

    temp = l[index1]
    l[index1] = l[index2]
    l[index2] = temp

    return l


# (d) N liczb float w kolejności losowej o rozkładzie gaussowskim
def random_gaussian_distribution_numbers_list(n):
    return list(np.random.randn(n))


# (e) N liczb int w kolejności losowej, o wartościach powtarzających się,
# należących do zbioru k elementowego (k < N, np. k*k = N).
def random_list_with_repeated_values(n):
    k = random.randint(1, n - 1)
    k_list = []
    for i in range(k):
        number = random.randint(0, n-1)
        while number in k_list:
            number = random.randint(0, n - 1)
        k_list.append(number)

    l = []
    k_list_len = len(k_list)
    for i in range(n):
        l.append(k_list[random.randint(0, k_list_len - 1)])

    return l


if __name__ == "__main__":
    print(random_unsorted_list(10))
    print(random_almost_sorted_list(10))
    print(random_almost_sorted_desc_order_list(10))
    print(random_gaussian_distribution_numbers_list(10))
    print(random_list_with_repeated_values(10))