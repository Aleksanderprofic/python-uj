# 4.2 Rozwiązania zadań 3.5 i 3.6 z poprzedniego zestawu zapisać w postaci funkcji,
# które zwracają pełny string przez return.

def create_ruler(length: int):
    if length < 0 or length > 20:
        raise Exception("Invalid argument! length has to be greater or equal to 0 " +
                        "and less than or equal to 20 (because otherwise it doesn't look nice)")
    ruler = "|"
    for i in range(length):
        ruler += "....|"

    ruler += "\n"
    ruler += "0"

    for i in range(1,length+1):
        ruler += "{:5d}".format(i)

    return ruler


print("Ruler with length 20: \n" + create_ruler(20))
print("Ruler with length 10: \n" + create_ruler(10))
print("Ruler with length 5: \n" + create_ruler(5))
print("Ruler with length 0: \n" + create_ruler(0))

print("\n")

def create_rectangle(x: int, y: int):
    if x < 0 or y < 0:
        raise Exception("Invalid argument! x and y have to be greater than 0")
    rectangle = "+"
    for i in range(y):
        rectangle += "---+"

    rectangle += "\n"
    for i in range(x):
        for j in range(y+1):
            rectangle += "{:4s}".format("|")
        rectangle += "\n+"

        for j in range(y):
            rectangle += "---+"
        rectangle += "\n"

    return rectangle


print("Rectangle 2x4: \n" + create_rectangle(2,4))
print("Rectangle 4x2: \n" + create_rectangle(4,2))
print("Rectangle 3x3: \n" + create_rectangle(3,3))
print("Rectangle 6x4: \n" + create_rectangle(6,4))


# 4.3 Napisać iteracyjną wersję funkcji factorial(n) obliczającej silnię.

def factorial(n: int) -> int:
    if n < 0:
        raise ValueError("Argument 'n' has to be greater or equal to 0")
    if n == 1 or n == 0:
        return 1
    result = 1
    for i in range(2, n+1):
        result *= i
    return result


assert 24 == factorial(4)
assert 3628800 == factorial(10)
assert 120 == factorial(5)
assert 121645100408832000 == factorial(19)
assert 2432902008176640000 == factorial(20)

# 4.4 Napisać iteracyjną wersję funkcji fibonacci(n) obliczającej n-ty wyraz ciągu Fibonacciego.

def fibonacci(n: int) -> int:
    if n < 0:
        raise ValueError("Argument 'n' has to be greater or equal to 0")
    if n == 0:
        return 0

    fib_previous = 0
    fib_next = 1
    for i in range(2, n+1):
        fib_temp = fib_previous + fib_next
        fib_previous = fib_next
        fib_next = fib_temp

    return fib_next

assert fibonacci(2) == 1
assert fibonacci(0) == 0
assert fibonacci(5) == 5
assert fibonacci(10) == 55
assert fibonacci(18) == 2584
assert fibonacci(19) == 4181

# 4.5 Napisać funkcję odwracanie(L, left, right) odwracającą kolejność elementów na liście
# od numeru left do right włącznie. Lista jest modyfikowana w miejscu (in place).
# Rozważyć wersję iteracyjną i rekurencyjną.

## Odwracanie iteracyjne

def odwracanie_iter(L: list, left: int, right: int):
    length = len(L)
    if length == 0:
        raise Exception("List doesn't have any values")
    if left < 0 or right < 0:
        raise ValueError("Arguments 'left' and 'right' have to be greater or equal to 0")
    if left > right:
        raise ValueError("Argument 'left' cannot be greater than 'right'")
    if left >= length or right >= length:
        raise ValueError("Arguments 'left' and 'right' cannot be greater than Lists length")
    while left < right:
        temp = L[left]; L[left] = L[right]; L[right] = temp
        left += 1; right -= 1


L1 = [1,2]
L2 = [1,2,3,4,5,6]
L3 = [1]
L4 = [1, -2, 55, 10, -1, 0, 0]

odwracanie_iter(L1, 0, 1)
assert L1 == [2, 1]
odwracanie_iter(L1, 0, 0)
assert L1 == [2, 1]
odwracanie_iter(L2, 3, 5)
assert L2 == [1,2,3,6,5,4]
odwracanie_iter(L2, 0, 4)
assert L2 == [5,6,3,2,1,4]
odwracanie_iter(L3, 0, 0)
assert L3 == [1]
odwracanie_iter(L4, 2, 5)
assert L4 == [1,-2,0,-1,10,55,0]
odwracanie_iter(L4, 0, 6)
assert L4 == [0,55,10,-1,0,-2,1]
odwracanie_iter(L4, 0, 3)
assert L4 == [-1,10,55,0,0,-2,1]


## Odwracanie rekurencyjne v1
def odwroc_rekurencyjnie1(L: list, left: int, right: int):
    if left < right:
        temp = L[left]
        L[left] = L[right]
        L[right] = temp
        odwroc_rekurencyjnie1(L, left + 1, right - 1)

def odwracanie_rekur1(L: list, left: int, right: int):
    length = len(L)
    if length == 0:
        raise Exception("List doesn't have any values")
    if left < 0 or right < 0:
        raise ValueError("Arguments 'left' and 'right' have to be greater or equal to 0")
    if left > right:
        raise ValueError("Argument 'left' cannot be greater than 'right'")
    if left >= length or right >= length:
        raise ValueError("Arguments 'left' and 'right' cannot be greater than Lists length")

    odwroc_rekurencyjnie1(L, left, right)

L1 = [1,2]
L2 = [1,2,3,4,5,6]
L3 = [1]
L4 = [1, -2, 55, 10, -1, 0, 0]

odwracanie_rekur1(L1, 0, 1)
assert L1 == [2, 1]
odwracanie_rekur1(L1, 0, 0)
assert L1 == [2, 1]
odwracanie_rekur1(L2, 3, 5)
assert L2 == [1,2,3,6,5,4]
odwracanie_rekur1(L2, 0, 4)
assert L2 == [5,6,3,2,1,4]
odwracanie_rekur1(L3, 0, 0)
assert L3 == [1]
odwracanie_rekur1(L4, 2, 5)
assert L4 == [1,-2,0,-1,10,55,0]
odwracanie_rekur1(L4, 0, 6)
assert L4 == [0,55,10,-1,0,-2,1]
odwracanie_rekur1(L4, 0, 3)
assert L4 == [-1,10,55,0,0,-2,1]

## Odwracanie rekurencyjne v2
def odwroc_rekurencyjnie2(L: list):
    length = len(L)
    if length > 1:
        temp = L[0]
        L[0] = L[-1]
        L[-1] = temp
        L[1:-1] = odwroc_rekurencyjnie2(L[1:-1])

    return L

def odwracanie_rekur2(L: list, left: int, right: int):
    length = len(L)
    if length == 0:
        raise Exception("List doesn't have any values")
    if left < 0 or right < 0:
        raise ValueError("Arguments 'left' and 'right' have to be greater or equal to 0")
    if left > right:
        raise ValueError("Argument 'left' cannot be greater than 'right'")
    if left >= length or right >= length:
        raise ValueError("Arguments 'left' and 'right' cannot be greater than Lists length")

    L[left:right + 1] = odwroc_rekurencyjnie2(L[left:right + 1])

L1 = [1,2]
L2 = [1,2,3,4,5,6]
L3 = [1]
L4 = [1, -2, 55, 10, -1, 0, 0]

odwracanie_rekur2(L1, 0, 1)
assert L1 == [2, 1]
odwracanie_rekur2(L1, 0, 0)
assert L1 == [2, 1]
odwracanie_rekur2(L2, 3, 5)
assert L2 == [1,2,3,6,5,4]
odwracanie_rekur2(L2, 0, 4)
assert L2 == [5,6,3,2,1,4]
odwracanie_rekur2(L3, 0, 0)
assert L3 == [1]
odwracanie_rekur2(L4, 2, 5)
assert L4 == [1,-2,0,-1,10,55,0]
odwracanie_rekur2(L4, 0, 6)
assert L4 == [0,55,10,-1,0,-2,1]
odwracanie_rekur2(L4, 0, 3)
assert L4 == [-1,10,55,0,0,-2,1]


# 4.6 Napisać funkcję sum_seq(sequence) obliczającą sumę liczb zawartych w sekwencji,
# która może zawierać zagnieżdżone podsekwencje. Wskazówka: rozważyć wersję rekurencyjną,
# a sprawdzanie, czy element jest sekwencją, wykonać przez isinstance(item, (list, tuple)).

def sum_seq(sequence) -> float:
    sum = 0

    for item in sequence:
        if isinstance(item, (list, tuple)):
            sum += sum_seq(item)
        else :
            sum += item

    return sum


seq1 = [1,(2,3),[],[4,(5,6,7)],8,[9]]
seq2 = [0,(-12,3,0,-10),[4,(5,6,(7))],8,0,[-9]]
seq3 = [([0, (-2)]), 5]
seq4 = []

assert sum_seq(seq1) == 45
assert sum_seq(seq2) == 2
assert sum_seq(seq3) == 3
assert sum_seq(seq4) == 0


# 4.7 Mamy daną sekwencję, w której niektóre z elementów mogą okazać się podsekwencjami,
# a takie zagnieżdżenia mogą się nakładać do nieograniczonej głębokości.
# Napisać funkcję flatten(sequence), która zwróci spłaszczoną listę wszystkich elementów sekwencji.
# Wskazówka: rozważyć wersję rekurencyjną, a sprawdzanie czy element jest sekwencją,
# wykonać przez isinstance(item, (list, tuple)).

# seq = [1,(2,3),[],[4,(5,6,7)],8,[9]]
# print flatten(seq)            # [1,2,3,4,5,6,7,8,9]

def flatten(sequence) -> list:
    flatten_seq: list = []

    for item in sequence:
        if isinstance(item, (list, tuple)):
            flatten_seq = flatten_seq + flatten(item)
        else :
            flatten_seq.append(item)

    return flatten_seq


seq1 = [1,(2,3),[],[4,(5,6,7)],8,[9]]
seq2 = [0,(-12,3,0,-10),[4,(5,6,(7))],8,0,[-9]]
seq3 = [([0, (-2)]), 5]
seq4 = []
seq5 = [(), [([],2)], ((()))]

assert flatten(seq1) == [1,2,3,4,5,6,7,8,9]
assert flatten(seq2) == [0,-12,3,0,-10,4,5,6,7,8,0,-9]
assert flatten(seq3) == [0,-2,5]
assert flatten(seq4) == []
assert flatten(seq5) == [2]
