import unittest
from zad4.queue import Queue


class TestQueue(unittest.TestCase):
    def setUp(self) -> None:
        self.queue = Queue()
        self.queue.put(1)
        self.queue.put(2)
        self.queue.put(3)

        self.queue2 = Queue()
        self.queue2.put(10)

        self.empty_queue = Queue()
        self.full_queue = Queue()
        self.full_queue.put(1)
        self.full_queue.put(2)
        self.full_queue.put(3)
        self.full_queue.put(4)
        self.full_queue.put(5)

    def test_str(self):
        self.assertEqual(str(self.empty_queue), "[]")
        self.assertEqual(str(self.queue), "[1, 2, 3]")
        self.assertEqual(str(self.queue2), "[10]")
        self.assertEqual(str(self.full_queue), "[1, 2, 3, 4, 5]")

    def test_is_empty(self):
        self.assertTrue(self.empty_queue.is_empty())
        self.assertFalse(self.queue.is_empty())
        self.assertFalse(self.queue2.is_empty())
        self.assertFalse(self.full_queue.is_empty())

    def test_is_full(self):
        self.assertFalse(self.empty_queue.is_full())
        self.assertFalse(self.queue.is_full())
        self.assertFalse(self.queue2.is_full())
        self.assertTrue(self.full_queue.is_full())

    def test_put(self):
        self.assertListEqual(self.queue.items, [1, 2, 3])
        self.assertListEqual(self.queue2.items, [10])
        self.assertListEqual(self.empty_queue.items, [])
        self.assertListEqual(self.full_queue.items, [1, 2, 3, 4, 5])

    def test_pop(self):
        self.queue.get()
        self.assertListEqual(self.queue.items, [2, 3])
        self.queue2.get()
        self.assertTrue(self.queue2.is_empty())
        self.full_queue.get()
        self.assertTrue(self.full_queue.items, [2, 3, 4, 5])
        with self.assertRaises(IndexError) as error:
            self.empty_queue.get()
        the_exception = error.exception
        self.assertEqual("Queue is empty!", str(the_exception))


if __name__ == "__main__":
    unittest.main()
