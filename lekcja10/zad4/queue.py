class Queue:
    def __init__(self):
        self.items = []

    def __str__(self):             # podglądanie kolejki
        return str(self.items)

    def is_empty(self):
        return not self.items

    # Zakładam, że kolejka ma maksymalną wielkość 5
    def is_full(self):
        return len(self.items) == 5

    def put(self, data):
        if self.is_full():
            raise IndexError("Queue is full!")
        self.items.append(data)

    def get(self):
        if self.is_empty():
            raise IndexError("Queue is empty!")
        return self.items.pop(0)
