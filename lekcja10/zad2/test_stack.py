import unittest
from zad2.stack import Stack


class TestStack(unittest.TestCase):
    def setUp(self) -> None:
        self.stack = Stack()
        self.stack.push(1)
        self.stack.push(2)
        self.stack.push(3)

        self.stack2 = Stack()
        self.stack2.push(10)

        self.empty_stack = Stack()

        self.full_stack = Stack()
        self.full_stack.push(1)
        self.full_stack.push(2)
        self.full_stack.push(3)
        self.full_stack.push(4)
        self.full_stack.push(5)

    def test_str(self):
        self.assertEqual(str(self.empty_stack), "[]")
        self.assertEqual(str(self.stack), "[1, 2, 3]")
        self.assertEqual(str(self.stack2), "[10]")
        self.assertEqual(str(self.full_stack), "[1, 2, 3, 4, 5]")

    def test_is_empty(self):
        self.assertTrue(self.empty_stack.is_empty())
        self.assertFalse(self.stack.is_empty())
        self.assertFalse(self.stack2.is_empty())
        self.assertFalse(self.full_stack.is_empty())

    def test_is_full(self):
        self.assertFalse(self.empty_stack.is_full())
        self.assertFalse(self.stack.is_full())
        self.assertFalse(self.stack2.is_full())
        self.assertTrue(self.full_stack.is_full())

    def test_push(self):
        self.assertListEqual(self.stack.items, [1, 2, 3])
        self.assertListEqual(self.stack2.items, [10])
        self.assertListEqual(self.empty_stack.items, [])
        self.assertListEqual(self.full_stack.items, [1, 2, 3, 4, 5])

    def test_pop(self):
        self.stack.pop()
        self.assertListEqual(self.stack.items, [1, 2])
        self.stack2.pop()
        self.assertTrue(self.stack2.is_empty())
        self.full_stack.pop()
        self.assertTrue(self.full_stack.items, [1, 2, 3, 4])
        with self.assertRaises(IndexError) as error:
            self.empty_stack.pop()
        the_exception = error.exception
        self.assertEqual("Stack is empty", str(the_exception))


if __name__ == "__main__":
    unittest.main()
