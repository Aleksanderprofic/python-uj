import random


class RandomQueue:
    def __init__(self):
        self.items = []

    def insert(self, item):
        if self.is_full():
            raise IndexError("Queue is full!")
        self.items.append(item)

    def remove(self):
        if self.is_empty():
            raise IndexError("Queue is empty!")
        index = random.randint(0, len(self.items) - 1)
        element = self.items[index]
        self.items[index] = self.items[-1]
        del self.items[-1]
        return element

    def is_empty(self):
        return not self.items

    # Zakładam, że kolejka ma maksymalną wielkość 5
    def is_full(self):
        return len(self.items) == 5

    def clear(self):
        self.items.clear()