import unittest
from zad8.random_queue import RandomQueue


class TestRandomQueue(unittest.TestCase):
    def setUp(self) -> None:
        self.queue = RandomQueue()
        self.queue.insert(1)
        self.queue.insert(2)
        self.queue.insert(3)

        self.queue2 = RandomQueue()
        self.queue2.insert(10)

        self.empty_queue = RandomQueue()

        self.full_queue = RandomQueue()
        self.full_queue.insert(1)
        self.full_queue.insert(2)
        self.full_queue.insert(3)
        self.full_queue.insert(4)
        self.full_queue.insert(5)

    def test_is_empty(self):
        self.assertTrue(self.empty_queue.is_empty())
        self.assertFalse(self.queue.is_empty())
        self.assertFalse(self.queue2.is_empty())
        self.assertFalse(self.full_queue.is_empty())

    def test_is_full(self):
        self.assertFalse(self.empty_queue.is_full())
        self.assertFalse(self.queue.is_full())
        self.assertFalse(self.queue2.is_full())
        self.assertTrue(self.full_queue.is_full())

    def test_insert(self):
        self.assertListEqual(self.queue.items, [1, 2, 3])
        self.assertListEqual(self.queue2.items, [10])
        self.assertListEqual(self.empty_queue.items, [])
        self.assertListEqual(self.full_queue.items, [1, 2, 3, 4, 5])

    def test_remove(self):
        self.queue.remove()
        self.assertEqual(len(self.queue.items), 2)
        self.queue2.remove()
        self.assertEqual(len(self.queue2.items), 0)
        self.full_queue.remove()
        self.assertEqual(len(self.full_queue.items), 4)
        with self.assertRaises(IndexError) as error:
            self.empty_queue.remove()
        the_exception = error.exception
        self.assertEqual("Queue is empty!", str(the_exception))

    def test_clear(self):
        self.queue.clear()
        self.assertTrue(self.queue.is_empty())
        self.queue2.clear()
        self.assertTrue(self.queue2.is_empty())
        self.full_queue.clear()
        self.assertTrue(self.full_queue.is_empty())
        self.empty_queue.clear()
        self.assertTrue(self.empty_queue.is_empty())


if __name__ == "__main__":
    unittest.main()
