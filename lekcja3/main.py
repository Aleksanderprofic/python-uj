# 3.1 Czy podany kod jest poprawny składniowo w Pythonie?
# 1 - TAK
# 2 - NIE
# 3 - NIE

# 3.2 Co jest złego w kodzie:
# 1 - funkcja sort() zwraca None, dlatego zmienna L zostanie nadpisana przez None
# 2 - za dużo elementów do przypisania, jeśli mamy dwie zmienne, to 2 wartości muszą być, żeby poprawnie zostały przypisane
# 3 - X jest krotką, dlatego nie można nadpisać już zapisanej w niej wartości, X[1] = 4 jest niepoprawne
# 4 - IndexError
# 5 - obiekt str nie ma zadnej funkcji append
# 6 - zwraca obiekt map, z którym nic nie można zrobić, pow powinno zostać
#     wywołane z dwoma argumentami, a potem z tego stworzona np. lista:
#     list(map(lambda x: pow(x, 2), range(8)))


# 3.3 Wypisać w pętli liczby od 0 do 30 z wyjątkiem liczb podzielnych przez 3.
for i in range(0,30):
    if i % 3 != 0:
        print(i)


# 3.4 Napisać program pobierający w pętli od użytkownika liczbę rzeczywistą x (typ float)
# i wypisujący parę x i trzecią potęgę x. Zatrzymanie programu następuje po wpisaniu
# z klawiatury stop. Jeżeli użytkownik wpisze napis zamiast liczby,
# to program ma wypisać komunikat o błędzie i kontynuować pracę.
x = ""
while True:
    x = input("Insert real number x: ")
    if x == "stop":
        break
    try:
        x = float(x)
        print("x: %f and x^3: %f" % (x, pow(x, 3)))
    except ValueError as e:
        print(e)


# 3.5 Napisać program rysujący "miarkę" o zadanej długości.
# Należy prawidłowo obsłużyć liczby składające się z kilku cyfr
# (ostatnia cyfra liczby ma znajdować się pod znakiem kreski pionowej).
# Należy zbudować pełny string, a potem go wypisać.
def create_ruler(length: int):
    if length < 0 or length > 20:
        raise Exception("Invalid argument! length has to be greater or equal to 0 " +
                        "and less than or equal to 20 (because otherwise it doesn't look nice)")
    ruler = "|"
    for i in range(length):
        ruler += "....|"

    ruler += "\n"
    ruler += "0"

    for i in range(1,length+1):
        ruler += "{:5d}".format(i)

    return ruler


print("Ruler with length 20: \n" + create_ruler(20))
print("Ruler with length 10: \n" + create_ruler(10))
print("Ruler with length 5: \n" + create_ruler(5))
print("Ruler with length 0: \n" + create_ruler(0))

print("\n")

# 3.6 Napisać program rysujący prostokąt zbudowany z małych kratek.
# Należy zbudować pełny string, a potem go wypisać.
# Przykładowy prostokąt składający się z 2x4 pól ma postać:
def create_rectangle(x: int, y: int):
    if x < 0 or y < 0:
        raise Exception("Invalid argument! x and y have to be greater than 0")
    rectangle = "+"
    for i in range(y):
        rectangle += "---+"

    rectangle += "\n"
    for i in range(x):
        for j in range(y+1):
            rectangle += "{:4s}".format("|")
        rectangle += "\n+"

        for j in range(y):
            rectangle += "---+"
        rectangle += "\n"

    return rectangle


print("Rectangle 2x4: \n" + create_rectangle(2,4))
print("Rectangle 4x2: \n" + create_rectangle(4,2))
print("Rectangle 3x3: \n" + create_rectangle(3,3))
print("Rectangle 6x4: \n" + create_rectangle(6,4))


# 3.8 Dla dwóch sekwencji znaleźć: (a) listę elementów występujących jednocześnie
# w obu sekwencjach (bez powtórzeń), (b) listę wszystkich elementów z obu sekwencji (bez powtórzeń).
### On sets
sequence1 = {1, "item", -5, (1, "hi"), "wow", 0}
sequence2 = {(1, "hi"), 1, "item", -22, -52, 5}

# a)
in_both_sequences = sequence1 & sequence2
assert in_both_sequences == {(1, 'hi'), 1, 'item'}

# b)
both_sequences = sequence1 | sequence2
assert both_sequences == {0, 1, 5, 'item', -22, -52, (1, 'hi'), 'wow', -5}

### On lists or tuples
sequence1 = (1, "item", -5, (1, "hi"), "wow", 0)
sequence2 = ((1, "hi"), 1, "item", -22, -52, 5)

in_both_sequences = []
both_sequences = list(sequence1)

# a)
for item in sequence1:
    if sequence2.__contains__(item):
        in_both_sequences.append(item)

assert in_both_sequences == [1, 'item', (1, 'hi')]

# b)
for item in sequence2:
    if not sequence1.__contains__(item):
        both_sequences.append(item)

assert both_sequences == [1, 'item', -5, (1, 'hi'), 'wow', 0, -22, -52, 5]

# 3.9 Mamy daną listę sekwencji (listy lub krotki) różnej długości zawierających liczby.
# Znaleźć listę zawierającą sumy liczb z tych sekwencji.
# Przykładowa sekwencja [[],[4],(1,2),[3,4],(5,6,7)], spodziewany wynik [0,4,3,7,18].
test1 = [[],[4],(1,2),[3,4],(5,6,7)]
test2 = [[11, -5],[3, 1, 2], ([1]), (), (3,4,5,6), (6,7)]
test3 = [(3, -6, 3), [1.7]]
test4 = [[2, 2, 2, 2], [0, 0, 0], (12, 0, 0)]
test5 = [([2])]

def compute_sum_of_sequences_from_list(L: list) -> list:
    return list(map(sum, L))

assert compute_sum_of_sequences_from_list(test1) == [0,4,3,7,18]
assert compute_sum_of_sequences_from_list(test2) == [6, 6, 1, 0, 18, 13]
assert compute_sum_of_sequences_from_list(test3) == [0, 1.7]
assert compute_sum_of_sequences_from_list(test4) == [8, 0, 12.0]
assert compute_sum_of_sequences_from_list(test5) == [2]

# 3.10 Stworzyć słownik tłumaczący liczby zapisane w systemie rzymskim
# (z literami I, V, X, L, C, D, M) na liczby arabskie
# (podać kilka sposobów tworzenia takiego słownika). Mile widziany kod tłumaczący całą liczbę [funkcja roman2int()].

def roman2int(roman: str) -> int:
    if not isinstance(roman, str):
        raise TypeError("Incorrect type of argument, got %s" % type(input))
    roman = roman.upper()
    nums = {
            'M':1000,
            'D':500,
            'C':100,
            'L':50,
            'X':10,
            'V':5,
            'I':1
    }
    sum = 0
    for i in range(len(roman)):
        try:
            value = nums[roman[i]]

            if i+1 < len(roman) and nums[roman[i+1]] > value:
                sum -= value
            else: sum += value
        except KeyError:
            raise ValueError('Incorrect roman string: %s' % roman)

    return sum


assert roman2int("XVI") == 16
assert roman2int("IVX") == 4
assert roman2int("XXIV") == 24
assert roman2int("XC") == 90
