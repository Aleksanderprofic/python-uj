class Node:
    """Klasa reprezentująca węzeł listy jednokierunkowej."""

    def __init__(self, data=None, next=None):
        self.data = data
        self.next = next

    def __str__(self):
        return str(self.data)


class SingleList:
    """Klasa reprezentująca całą listę jednokierunkową."""

    def __init__(self):
        self.length = 0
        self.head = None
        self.tail = None

    def __str__(self):
        temp = self.head
        data_array = []
        while temp is not None:
            data_array.append(temp.data)
            temp = temp.next
        return str(data_array)

    def is_empty(self):
        return self.length == 0

    def count(self):
        return self.length

    def insert_head(self, node):
        if self.length == 0:
            self.head = self.tail = node
        else:
            node.next = self.head
            self.head = node
        self.length += 1

    def insert_tail(self, node):  # klasy O(N)
        if self.length == 0:
            self.head = self.tail = node
        else:  # dajemy na koniec listy
            self.tail.next = node
            self.tail = node
        self.length += 1

    def remove_head(self):  # klasy O(1)
        if self.length == 0:
            raise ValueError("pusta lista")
        node = self.head
        if self.head == self.tail:  # self.length == 1
            self.head = self.tail = None
        else:
            self.head = self.head.next
        node.next = None  # czyszczenie łącza
        self.length -= 1
        return node  # zwracamy usuwany node

    def remove_tail(self):
        if self.length == 0:
            raise ValueError("Lista jest pusta")
        node = self.tail
        if self.head == self.tail:
            self.head = self.tail = None
        else:
            temp = self.head
            while temp.next != self.tail:
                temp = temp.next
            self.tail = temp
            self.tail.next = temp.next
        node.next = None
        self.length -= 1
        return node

    def merge(self, other):
        if not isinstance(other, SingleList):
            raise ValueError("Argument 'other' has to be a SingleList instance")

        if self.length == 0:
            self.head = other.head
            self.tail = other.tail
            self.length = other.length
        elif self.head == self.tail:
            self.head.next = other.head
            self.tail = other.tail
            self.length += other.length
        else:
            self.tail.next = other.head
            self.tail = other.tail
            self.length += other.length
        return self

    def clear(self):
        self.head = self.tail = None
        self.length = 0
        return self


if __name__ == "__main__":
    first_list = SingleList()
    first_list.insert_head(Node(11))  # [11]
    first_list.insert_head(Node(22))  # [22, 11]
    first_list.insert_tail(Node(33))  # [22, 11, 33]

    assert first_list.remove_tail().data == 33
    assert first_list.remove_tail().data == 11
    assert first_list.remove_tail().data == 22

    assert first_list.is_empty() is True

    first_list.insert_head(Node(11))  # [11]
    first_list.insert_head(Node(22))  # [22, 11]
    first_list.insert_tail(Node(33))  # [22, 11, 33]

    second_list = SingleList()
    second_list.insert_head(Node(1))  # [1]
    second_list.insert_head(Node(2))  # [2, 1]
    second_list.insert_tail(Node(3))  # [2, 1, 3]
    assert str(first_list.merge(second_list)) == '[22, 11, 33, 2, 1, 3]'

    empty_list = SingleList()
    assert str(empty_list.merge(second_list)) == '[2, 1, 3]'

    one_element_list = SingleList()
    one_element_list.insert_head(Node(1))

    assert str(one_element_list.merge(second_list)) == '[1, 2, 1, 3]'

    assert str(one_element_list.clear()) == '[]'
