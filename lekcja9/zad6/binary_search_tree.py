class Node:
    """Klasa reprezentująca węzeł drzewa binarnego."""

    def __init__(self, data=None, left=None, right=None):
        self.data = data
        self.left = left
        self.right = right

    def __str__(self):
        return str(self.data)


def count_leafs(top: Node):
    if top is None:
        return 0
    if not isinstance(top, Node):
        raise ValueError("Argument 'top' has to be of type Node")

    if top.left is None and top.right is None:
        return 1

    return count_leafs(top.left) + count_leafs(top.right)


def count_total(top):
    if top is None:
        return 0
    if not isinstance(top, Node):
        raise ValueError("Argument 'top' has to be of type Node")

    return top.data + count_total(top.left) + count_total(top.right)


root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)
root.right.left = Node(6)
root.right.right = Node(7)

assert count_leafs(root) == 4
assert count_total(root) == 28
