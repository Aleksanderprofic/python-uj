import unittest
from zad2.points import Point


class TestPoint(unittest.TestCase):

    def test_print(self):
        self.assertEqual(repr(Point(1, 2)), "Point(1,2)")
        self.assertEqual(str(Point(1, 2)), "(1,2)")

    def test_eq(self):
        self.assertTrue(Point(1, 2) == Point(1, 2))
        self.assertFalse(Point(2, 2) == Point(1, 2))

    def test_ne(self):
        self.assertTrue(Point(2, 2) != Point(1, 2))
        self.assertFalse(Point(1, 2) != Point(1, 2))

    def test_add(self):
        self.assertEqual(Point(2, 2) + Point(3, 1), Point(5, 3))
        self.assertEqual(Point(-2, -2) + Point(3, 1), Point(1, -1))

    def test_sub(self):
        self.assertEqual(Point(2, 2) - Point(3, 1), Point(-1, 1))
        self.assertEqual(Point(-2, -2) - Point(3, 1), Point(-5, -3))

    def test_mul(self):
        self.assertEqual(Point(2, 3) * Point(3, 4), 18)
        self.assertEqual(Point(0, 1) * Point(-1, -4), -4)

    def test_cross(self):
        self.assertEqual(Point(2, 2).cross(Point(1, 5)), 8)

    def test_length(self):
        self.assertEqual(Point(3, 4).length(), 5)


if __name__ == "__main__":
    unittest.main()
