import unittest
from zad1.times import Time


class TestTime(unittest.TestCase):

    def setUp(self): pass

    def test_print(self):
        self.assertEqual(repr(Time(5)), "Time(5)")
        self.assertEqual(repr(Time(20)), "Time(20)")
        self.assertEqual(str(Time(61)), "00:01:01")
        self.assertEqual(str(Time(15)), "00:00:15")
        self.assertEqual(str(Time(3661)), "01:01:01")
        self.assertEqual(str(Time(7000)), "01:56:40")

    def test_add(self):
        self.assertEqual(Time(1) + Time(2), Time(3))
        self.assertEqual(Time(120) + Time(200), Time(320))

    def test_compare(self):
        self.assertTrue(Time(1) == Time(1))
        self.assertFalse(Time(2) == Time(1))
        self.assertTrue(Time(1) != Time(2))
        self.assertFalse(Time(1) != Time(1))
        self.assertTrue(Time(3) > Time(2))
        self.assertFalse(Time(2) > Time(3))
        self.assertTrue(Time(1) < Time(2))
        self.assertFalse(Time(2) < Time(1))
        self.assertTrue(Time(2) >= Time(1))
        self.assertTrue(Time(1) >= Time(1))
        self.assertFalse(Time(1) >= Time(2))
        self.assertTrue(Time(2) <= Time(3))
        self.assertTrue(Time(2) <= Time(2))
        self.assertFalse(Time(3) <= Time(2))

    def test_int(self):
        self.assertEqual(int(Time(100)), 100)
        self.assertEqual(int(Time(276)), 276)
        self.assertEqual(int(Time()), 0)

    def tearDown(self): pass


if __name__ == "__main__":
    unittest.main()  # wszystkie testy
