# Projekt zaliczeniowy - Słownik T9

Słownik T9 (od ang. Text on 9 keys) – słownik wykorzystywany przy tzw. metodzie słownikowej wpisywania tekstu w telefonie komórkowym. Opatentowana przez Tegic Communications. Metoda słownikowa przyspiesza wpisywanie tekstu na klawiaturze o 8 przyciskach znaczących. By wpisać słowo naciska się odpowiednie klawisze jednokrotnie.

Przykład: standardowo, aby wpisać słowo trawniki, trzeba nacisnąć klawisze 16 razy (kolejno: 8777296644455444), natomiast przy pomocy słownika T9 tylko 8 razy (87296454).

Link do szerszego opisu na angielskiej [Wikipedii](https://en.wikipedia.org/wiki/T9_%28predictive_text%29)


## Wymagania odnośnie uruchomienia programu

- Python w wersji 3.7 lub wyżej

## Uruchomienie programu:

Aby uruchomić program należy skopiować pliki do jednego folderu (razem z plikiem `slowa.txt`) oraz wpisać komendę `python3 t9.py` w terminalu.

## Jak działa program:

Po uruchomieniu programu pojawi się narysowany telefon wraz z przyciskami oraz odpowiadającymi im literami i liczbami. Aby wyszukac interesujące nas słowo należy wpisywać cyfry odpowiadające kolejnym literom (np. 2 dla liter a, b lub c).

## Funkcje poszczegolnych przycisków:

- od `2` do `9` dodaje literę do aktualnie istniejącego tekstu 
- `1` - Wypisuje wszystkie wyrazy, które może utworzyć z aktualnie podanych liter (muszą być conajmniej 3)
- `#` - zamienia aktualny tekst na kolejny sugerowany wyraz (z podpowiedzi)
- `0` - resetuje do stanu początkowego
- `*` - wypisuje informacje o programie

## Kod zrodłowy:

- Klasa `TrieNode` - odpowiada za węzeł w drzewie
    - `nodes` przechowuje 8 referencji do dalszej części drzewa (każda odpowiada cyfrze na telefonie od 2 do 9)
    - `words` przechowuje wyrazy, ktore kończą się na danym węzle
- Klasa `T9Dictionary` - implementuje wszystkie metody związane z działaniami na drzewie
