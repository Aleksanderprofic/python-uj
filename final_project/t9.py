from trie_node import TrieNode
import random
import time


class T9Dictionary:
    def __init__(self, file_name='slowa.txt'):
        self.__endOfSuggestions = False
        self.__which_word = 0
        self.__letter_to_node_number_dict = {'a': 0, 'b': 0, 'c': 0,
                                             'd': 1, 'e': 1, 'f': 1,
                                             'g': 2, 'h': 2, 'i': 2,
                                             'j': 3, 'k': 3, 'l': 3,
                                             'm': 4, 'n': 4, 'o': 4,
                                             'p': 5, 'q': 5, 'r': 5, 's': 5,
                                             't': 6, 'u': 6, 'v': 6,
                                             'w': 7, 'x': 7, 'y': 7, 'z': 7}
        self.__sugg = ['', '']
        self.__actual_text = ""
        self.__root_node = TrieNode()
        self.__actual_node = self.__root_node
        self.__temp_node = self.__root_node
        self.__load_words(file_name)

    @staticmethod
    def run():
        print("\n\nBelow are shown the possible actions to perform. "
              "To do something, enter the button from your keyboard ")
        print("and to end the program, simply enter: 'end'\n")
        result = None
        while not end:
            T9Dictionary.__display_phone()
            if result is not None:
                print(f"\nActual text is: {result}")

            button = input('Enter the button: ')
            if button == 'end':
                print("Ending program...")
                break
            result = dictionary.click(button)

            if result is not None:
                print(f"\nActual text is: {result}")

            print("\n\nMoving on after 3 secs...")
            time.sleep(3)

    @staticmethod
    def __display_phone():
        print(' _________________')
        print('|                 |')
        print('|                 |')
        print('|                 |')
        print('|                 |')
        print('|-----------------|')
        print('|  1  |  2  |  3  |')
        print('|     | abc | def |')
        print('|-----------------|')
        print('|  4  |  5  |  6  |')
        print('| ghi | jkl | mno |')
        print('|-----------------|')
        print('|  7  |  8  |  9  |')
        print('| pqrs| tuv |wxyz |')
        print('|-----------------|')
        print('|  *  |  0  |  #  |')
        print(' -----------------')

    @staticmethod
    def __go_deeper(node, k, creating_new_nodes=True):
        if creating_new_nodes:
            if node.nodes[k] is None:
                node.nodes[k] = TrieNode()
            return node.nodes[k]

        if node is None:
            return None
        else:
            return node.nodes[k]

    def __load_words(self, file_name):
        f = open(file_name)
        lines = f.readlines()
        for line in lines:
            self.__insert(line)

    def __insert(self, word: str):
        word_length = len(word)
        if word_length > 0:
            word = word.strip()
            for letter in word:
                if letter in self.__letter_to_node_number_dict:
                    node_number = self.__letter_to_node_number_dict[letter]
                    self.__actual_node = self.__go_deeper(self.__actual_node, node_number)
                else:
                    self.__actual_node = self.__root_node
                    return

            self.__actual_node.add_word(word)
            self.__actual_node = self.__root_node

    def __traverse(self, actual):
        for word in actual.words:
            print(word)
        for node in actual.nodes:
            if node is not None:
                self.__traverse(node)

    def __perform_action(self, number):
        self.__endOfSuggestions = False
        self.__actual_node = self.__go_deeper(self.__actual_node, number, False)

        if self.__actual_node is not None:
            self.__temp_node = self.__actual_node

            if len(self.__actual_node.words) == 0:
                self.__sugg[0] = ""
                self.__sugg[1] = ""
                table = ["abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"]

                if len(table[number]) == 3:
                    rand = random.randint(0, 2)
                else:
                    rand = random.randint(0, 3)

                if rand == 0:
                    self.__actual_text += table[number][0]
                elif rand == 1:
                    self.__actual_text += table[number][1]
                elif rand == 2:
                    self.__actual_text += table[number][2]
                else:
                    self.__actual_text += table[number][3]

                self.__which_word = 0
            else:
                self.__actual_text = self.__actual_node.words[0]
                self.__which_word = 1

            self.__suggestions(self.__temp_node)
            return self.__actual_text
        else:
            print("Nie ma takiego słowa. Sprobuj od nowa!")
            self.__reset()

    def __suggestions(self, temp, first_letter=False):
        if temp is None:
            return False

        end = False
        if len(temp.words) > self.__which_word:
            if not first_letter:
                self.__sugg[0] = temp.words[self.__which_word]
                self.__which_word += 1
                print(f"Podpowiedz nr 1: {self.__sugg[0]}")
                first_letter = True
                self.__temp_node = temp
            if len(temp.words) > self.__which_word:
                self.__sugg[1] = temp.words[self.__which_word]
                print(f"Podpowiedz nr 2: {self.__sugg[1]}")
                self.__temp_node = temp
                return True

        i = 0
        while not end and i < 8 and temp is not None:
            temp2 = None
            while temp2 is None and i < 8:
                self.__which_word = 0
                temp2 = self.__find_next_node_with_strings(temp.nodes[i])
                i += 1

            if temp2 is not None:
                if not first_letter and len(temp2.words) > self.__which_word:
                    first_letter = True
                    self.__sugg[0] = temp2.words[self.__which_word]
                    self.__which_word += 1
                    print(f"Podpowiedz nr 1: {self.__sugg[0]}")
                    self.__temp_node = temp2

                if len(temp2.words) > self.__which_word:
                    self.__sugg[1] = temp2.words[self.__which_word]
                    print(f"Podpowiedz nr 2: {self.__sugg[1]}")
                    self.__temp_node = temp2
                    return True
                end = self.__suggestions(temp2, first_letter)
        if end:
            return True
        return False

    def __find_next_node_with_strings(self, node):
        if node is not None:
            if not len(node.words) == 0:
                return node
            tmp = None
            i = 0
            while tmp is None and i < 8:
                tmp = self.__find_next_node_with_strings(node.nodes[i])
                i += 1
            return tmp
        return None

    # Akcja gdy wcisniemy # -> wypisanie nastepnego slowa
    def __when_hash_clicked(self):
        if len(self.__sugg[0]) > 0:
            self.__actual_text = self.__sugg[0]

            print(f"Nastepne slowo: {self.__actual_text}")

            if not self.__endOfSuggestions:
                if not self.__suggestions(self.__temp_node):
                    self.__endOfSuggestions = True
                else:
                    self.__actual_node = self.__temp_node
            else:
                self.__actual_node = None
                print(f"Nastepne slowo: {self.__sugg[0]}")

    def __reset(self):
        self.__endOfSuggestions = False
        self.__which_word = 0
        self.__sugg[0] = ""
        self.__sugg[1] = ""
        self.__actual_text = ""
        self.__actual_node = self.__root_node
        self.__temp_node = self.__root_node

    def click(self, but):
        if but == '2':
            return self.__perform_action(0)
        elif but == '3':
            return self.__perform_action(1)
        elif but == '4':
            return self.__perform_action(2)
        elif but == '5':
            return self.__perform_action(3)
        elif but == '6':
            return self.__perform_action(4)
        elif but == '7':
            return self.__perform_action(5)
        elif but == '8':
            return self.__perform_action(6)
        elif but == '9':
            return self.__perform_action(7)
        elif but == '0':
            self.__reset()
        elif but == '*':
            print("\nThis T9 Dictionary was created by Aleksander Profic\n")
        elif but == '#':
            self.__when_hash_clicked()
        elif but == '1':
            if len(self.__actual_text) >= 3:
                if self.__actual_node is not None:
                    print("Lista wszystkich pasujących słów:")
                    self.__traverse(self.__actual_node)
                else:
                    print("Nie znalazłem żadnego pasującego słowa. Sprobuj od nowa!")
                self.__reset()
            else:
                print("Uzyj tego przycisku, kiedy bedziesz mial juz co najmniej 3 litery w slowie")
        else:
            print("Niedozwolona akcja! Spróbuj ponownie!")


if __name__ == "__main__":
    end = False
    dictionary = T9Dictionary('slowa_1k.txt')

    dictionary.run()
