class TrieNode:
    def __init__(self):
        self.nodes = [None] * 8
        self.words = []

    def add_word(self, word):
        if word not in self.words:
            self.words.append(word)

