import unittest
from zad1.fracs import Frac


class TestFrac(unittest.TestCase):

    def setUp(self) -> None:
        self.frac1 = Frac(1, 2)
        self.frac2 = Frac(1, 3)
        self.frac3 = Frac(-1, 2)
        self.frac4 = Frac(-1, 3)

        self.frac5 = Frac(1, -2)
        self.frac6 = Frac(1, -3)

        self.frac7 = Frac(4, 8)

    def test_init(self):
        self.assertEqual(Frac(4, 8), self.frac1)
        self.assertEqual(Frac(-4, 8), self.frac3)
        self.assertEqual(Frac(4, -8), self.frac3)
        self.assertEqual(Frac(-4, -8), self.frac1)

        self.assertEqual(Frac(1, 2), self.frac1)
        self.assertEqual(Frac(-1, 2), self.frac3)
        self.assertEqual(Frac(1, -2), self.frac3)
        self.assertEqual(Frac(-1, -2), self.frac1)

    def test_str(self):
        self.assertEqual(str(self.frac1), "1/2")
        self.assertEqual(str(self.frac3), "-1/2")
        self.assertEqual(str(self.frac6), "-1/3")
        self.assertEqual(str(self.frac7), "1/2")

    def test_repr(self):
        self.assertEqual(repr(self.frac1), "Frac(1,2)")
        self.assertEqual(repr(self.frac2), "Frac(1,3)")
        self.assertEqual(repr(self.frac3), "Frac(-1,2)")
        self.assertEqual(repr(self.frac4), "Frac(-1,3)")
        self.assertEqual(repr(self.frac5), "Frac(-1,2)")
        self.assertEqual(repr(self.frac6), "Frac(-1,3)")
        self.assertEqual(repr(self.frac7), "Frac(1,2)")

    def test_eq(self):
        self.assertFalse(self.frac1 == self.frac2)
        self.assertTrue(self.frac1 == self.frac7)
        self.assertTrue(self.frac3 == self.frac5)
        self.assertTrue(self.frac1 == self.frac1)

    def test_ne(self):
        self.assertTrue(self.frac1 != self.frac2)
        self.assertFalse(self.frac1 != self.frac7)
        self.assertFalse(self.frac3 != self.frac5)
        self.assertFalse(self.frac1 != self.frac1)

    def test_lt(self):
        self.assertFalse(self.frac1 < self.frac2)
        self.assertFalse(self.frac1 < self.frac7)
        self.assertFalse(self.frac3 < self.frac5)
        self.assertTrue(self.frac3 < self.frac1)

    def test_le(self):
        self.assertFalse(self.frac1 <= self.frac2)
        self.assertTrue(self.frac1 <= self.frac7)
        self.assertTrue(self.frac3 <= self.frac5)
        self.assertTrue(self.frac1 <= self.frac1)
        self.assertTrue(self.frac3 <= self.frac1)

    def test_gt(self):
        self.assertTrue(self.frac1 > self.frac2)
        self.assertFalse(self.frac1 > self.frac7)
        self.assertFalse(self.frac3 > self.frac5)
        self.assertFalse(self.frac3 > self.frac1)

    def test_ge(self):
        self.assertTrue(self.frac1 >= self.frac2)
        self.assertTrue(self.frac1 >= self.frac7)
        self.assertTrue(self.frac3 >= self.frac5)
        self.assertFalse(self.frac3 >= self.frac1)

    def test_add(self):
        self.assertEqual(self.frac1 + self.frac2, Frac(5, 6))
        self.assertEqual(self.frac3 + self.frac2, Frac(-1, 6))
        self.assertEqual(self.frac1 + self.frac4, Frac(1, 6))
        self.assertEqual(self.frac3 + self.frac4, Frac(-5, 6))
        self.assertEqual(self.frac5 + self.frac2, Frac(-1, 6))
        self.assertEqual(self.frac1 + self.frac6, Frac(1, 6))
        self.assertEqual(self.frac1 + self.frac1, Frac(1, 1))
        self.assertEqual(self.frac1 + self.frac5, Frac(0, 1))

    def test_radd(self):
        self.assertEqual(1 + self.frac1, Frac(3, 2))
        self.assertEqual(1 + self.frac2, Frac(4, 3))
        self.assertEqual(1 + self.frac3, Frac(1, 2))
        self.assertEqual(1 + self.frac4, Frac(2, 3))
        self.assertEqual(1 + self.frac5, Frac(1, 2))
        self.assertEqual(1 + self.frac6, Frac(2, 3))
        self.assertEqual(1 + self.frac7, Frac(3, 2))

    def test_sub(self):
        self.assertEqual(self.frac1 - self.frac2, Frac(1, 6))
        self.assertEqual(self.frac3 - self.frac2, Frac(-5, 6))
        self.assertEqual(self.frac1 - self.frac4, Frac(5, 6))
        self.assertEqual(self.frac3 - self.frac4, Frac(-1, 6))
        self.assertEqual(self.frac5 - self.frac2, Frac(-5, 6))
        self.assertEqual(self.frac1 - self.frac6, Frac(5, 6))
        self.assertEqual(self.frac1 - self.frac1, Frac(0, 1))
        self.assertEqual(self.frac1 - self.frac5, Frac(1, 1))

    def test_rsub(self):
        self.assertEqual(1 - self.frac1, Frac(1, 2))
        self.assertEqual(1 - self.frac2, Frac(2, 3))
        self.assertEqual(1 - self.frac3, Frac(3, 2))
        self.assertEqual(1 - self.frac4, Frac(4, 3))
        self.assertEqual(1 - self.frac5, Frac(3, 2))
        self.assertEqual(1 - self.frac6, Frac(4, 3))
        self.assertEqual(1 - self.frac7, Frac(1, 2))

    def test_mul(self):
        self.assertEqual(self.frac1 * self.frac2, Frac(1, 6))
        self.assertEqual(self.frac3 * self.frac2, Frac(-1, 6))
        self.assertEqual(self.frac1 * self.frac4, Frac(-1, 6))
        self.assertEqual(self.frac3 * self.frac4, Frac(1, 6))
        self.assertEqual(self.frac5 * self.frac2, Frac(-1, 6))
        self.assertEqual(self.frac1 * self.frac6, Frac(-1, 6))
        self.assertEqual(self.frac1 * self.frac1, Frac(1, 4))
        self.assertEqual(self.frac1 * self.frac5, Frac(-1, 4))

    def test_rmul(self):
        self.assertEqual(3 * self.frac1, Frac(3, 2))
        self.assertEqual(3 * self.frac2, Frac(1, 1))
        self.assertEqual(3 * self.frac3, Frac(-3, 2))
        self.assertEqual(3 * self.frac4, Frac(-1, 1))
        self.assertEqual(3 * self.frac5, Frac(-3, 2))
        self.assertEqual(3 * self.frac6, Frac(-1, 1))
        self.assertEqual(3 * self.frac7, Frac(3, 2))

    def test_truediv(self):
        self.assertEqual(self.frac1 / self.frac2, Frac(3, 2))
        self.assertEqual(self.frac3 / self.frac2, Frac(-3, 2))
        self.assertEqual(self.frac1 / self.frac4, Frac(-3, 2))
        self.assertEqual(self.frac3 / self.frac4, Frac(3, 2))
        self.assertEqual(self.frac5 / self.frac2, Frac(-3, 2))
        self.assertEqual(self.frac1 / self.frac6, Frac(-3, 2))
        self.assertEqual(self.frac1 / self.frac1, Frac(1, 1))
        self.assertEqual(self.frac1 / self.frac5, Frac(-1, 1))

    def test_rtruediv(self):
        self.assertEqual(3 / self.frac1, Frac(6, 1))
        self.assertEqual(3 / self.frac2, Frac(9, 1))
        self.assertEqual(3 / self.frac3, Frac(-6, 1))
        self.assertEqual(3 / self.frac4, Frac(-9, 1))
        self.assertEqual(3 / self.frac5, Frac(-6, 1))
        self.assertEqual(3 / self.frac6, Frac(-9, 1))
        self.assertEqual(3 / self.frac7, Frac(6, 1))

    def test_pos(self):
        self.assertEqual(+self.frac1, self.frac1)
        self.assertEqual(+self.frac2, self.frac2)
        self.assertEqual(+self.frac3, self.frac3)
        self.assertEqual(+self.frac4, self.frac4)
        self.assertEqual(+self.frac5, self.frac5)
        self.assertEqual(+self.frac6, self.frac6)
        self.assertEqual(+self.frac7, self.frac7)

    def test_neg(self):
        self.assertEqual(-self.frac1, Frac(-1, 2))
        self.assertEqual(-self.frac2, Frac(-1, 3))
        self.assertEqual(-self.frac3, Frac(1, 2))
        self.assertEqual(-self.frac4, Frac(1, 3))
        self.assertEqual(-self.frac5, Frac(1, 2))
        self.assertEqual(-self.frac6, Frac(1, 3))
        self.assertEqual(-self.frac7, Frac(-1, 2))

    def test_invert(self):
        self.assertEqual(~self.frac1, Frac(2, 1))
        self.assertEqual(~self.frac2, Frac(3, 1))
        self.assertEqual(~self.frac3, Frac(-2, 1))
        self.assertEqual(~self.frac4, Frac(-3, 1))
        self.assertEqual(~self.frac5, Frac(-2, 1))
        self.assertEqual(~self.frac6, Frac(-3, 1))
        self.assertEqual(~self.frac7, Frac(2, 1))

    def test_float(self):
        self.assertAlmostEqual(float(self.frac1), 0.5)
        self.assertEqual(float(Frac(0, 2)), 0)


if __name__ == '__main__':
    unittest.main()
