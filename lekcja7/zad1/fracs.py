# W pliku fracs.py zdefiniować klasę Frac wraz z potrzebnymi metodami. Wykorzystać wyjątek ValueError do obsługi
# błędów w ułamkach. Dodać możliwości dodawania liczb (int, long) do ułamków (działania lewostronne i prawostronne).
# Rozważyć możliwość włączenia liczb float do działań na ułamkach [Wskazówka: metoda float.as_integer_ratio()].
# Napisać kod testujący moduł fracs.

import math
import numpy as np


class Frac:
    """Klasa reprezentująca ułamki."""

    def __init__(self, x=0, y=1):
        if y == 0:
            raise ValueError("Denominator cannot be equal to zero")
        self.x = x
        self.y = y
        self.__shortening()
        self.__move_minus_sign_to_nominator()

    def __str__(self):
        if self.y == 1:
            return str(self.x)

        return f"{self.x}/{self.y}"

    def __repr__(self):
        return f"Frac({self.x},{self.y})"

    def __eq__(self, other):
        if self.y == other.y:
            return self.x == other.x
        else:
            self_nominator, other_nominator = self.__get_nominators_from_reduction_to_a_common_denominator(other)
            return self_nominator == other_nominator

    def __ne__(self, other):
        return not (self == other)

    def __lt__(self, other):
        if self.y == other.y:
            return self.x < other.x
        else:
            self_nominator, other_nominator = self.__get_nominators_from_reduction_to_a_common_denominator(other)
            return self_nominator < other_nominator

    def __le__(self, other):
        if self.y == other.y:
            return self.x <= other.x
        else:
            self_nominator, other_nominator = self.__get_nominators_from_reduction_to_a_common_denominator(other)
            return self_nominator <= other_nominator

    def __gt__(self, other):
        if self.y == other.y:
            return self.x > other.x
        else:
            self_nominator, other_nominator = self.__get_nominators_from_reduction_to_a_common_denominator(other)
            return self_nominator > other_nominator

    def __ge__(self, other):
        if self.y == other.y:
            return self.x >= other.x
        else:
            self_nominator, other_nominator = self.__get_nominators_from_reduction_to_a_common_denominator(other)
            return self_nominator >= other_nominator

    def __add__(self, other):
        if not isinstance(other, (Frac, int)):
            raise ValueError("You can only add integers and other Frac objects to Frac object")
        if isinstance(other, Frac):
            if self.y == other.y:
                return Frac(self.x + other.x, self.y)
            else:
                lcm = np.lcm(self.y, other.y)
                return Frac(self.x * lcm // self.y + other.x * lcm // other.y, lcm)
        if isinstance(other, int):
            return Frac(self.x+self.y*other, self.y)

    __radd__ = __add__  # int+frac

    def __sub__(self, other):
        if not isinstance(other, (Frac, int)):
            raise ValueError("You can only subtract integers and other Frac objects from Frac object")
        if isinstance(other, Frac):
            if self.y == other.y:
                return Frac(self.x - other.x, self.y)
            else:
                lcm = np.lcm(self.y, other.y)
                return Frac(self.x * lcm // self.y - other.x * lcm // other.y, lcm)
        if isinstance(other, int):
            return Frac(self.x - self.y * other, self.y)

    def __rsub__(self, other):
        return Frac(self.y * other - self.x, self.y)

    def __mul__(self, other):
        if not isinstance(other, (Frac, int)):
            raise ValueError("You can only multiply Frac objects by integers and other Frac objects")
        if isinstance(other, Frac):
            return Frac(self.x * other.x, self.y * other.y)
        if isinstance(other, int):
            return Frac(self.x * other, self.y)

    __rmul__ = __mul__

    def __truediv__(self, other):
        if not isinstance(other, (Frac, int)):
            raise ValueError("You can only divide Frac objects by integers and other Frac objects")
        if isinstance(other, Frac):
            return Frac(self.x * other.y, self.y * other.x)
        if isinstance(other, int):
            return Frac(self.x, self.y * other)

    def __rtruediv__(self, other):
        return Frac(other * self.y, self.x)

    def __pos__(self):  # +frac = (+1)*frac
        return self

    def __neg__(self):
        return (-1)*Frac(self.x, self.y)

    def __invert__(self):
        return Frac(self.y, self.x)

    def __float__(self):
        return self.x / self.y

    def __get_nominators_from_reduction_to_a_common_denominator(self, other):
        lcm = np.lcm(self.y, other.y)
        return self.x * lcm // self.y,  other.x * lcm // other.y

    def __move_minus_sign_to_nominator(self):
        if self.y < 0:
            self.x = -self.x
            self.y = -self.y

    def __shortening(self):
        gcd = math.gcd(self.x, self.y)

        if gcd != 1:
            self.x = self.x // gcd
            self.y = self.y // gcd
