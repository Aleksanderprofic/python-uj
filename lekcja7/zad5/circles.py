# W pliku circles.py zdefiniować klasę Circle wraz z potrzebnymi metodami.
# Okrąg jest określony przez podanie środka i promienia.
# Wykorzystać wyjątek ValueError do obsługi błędów.
# Napisać kod testujący moduł circles.

from zad5.points import Point
import numpy as np


class Circle:
    def __init__(self, x, y, r):
        if r < 0:
            raise ValueError("Circle's radius can't be less than 0")
        self.center = Point(x, y)
        self.r = r

    def __repr__(self):
        return f"Circle({self.center.x}, {self.center.y}, {self.r})"

    def __eq__(self, other):
        if not isinstance(other, Circle):
            raise ValueError("Second argument must be a Circle.")

        return self.center == other.center and self.r == other.r

    def __ne__(self, other):
        return not self == other

    def area(self):
        return "%.4f" % (np.pi * self.r ** 2)

    def move(self, x, y):
        self.center = Point(self.center.x + x, self.center.y + y)
        return self

    def cover(self, other):
        if not isinstance(other, Circle):
            raise ValueError("Second argument must be a Circle.")

        cover_circle = Circle(self.center.x, self.center.y, self.r) if self.r > other.r else Circle(other.center.x,
                                                                                                    other.center.y,
                                                                                                    other.r)
        center_x = (self.center.x + other.center.x) // 2
        center_y = (self.center.y + other.center.y) // 2
        equation = round(np.sqrt((cover_circle.center.x - center_x) ** 2 + (cover_circle.center.y - center_y) ** 2), 2)

        cover_circle.center = Point(center_x, center_y)
        cover_circle.r += equation

        return cover_circle
