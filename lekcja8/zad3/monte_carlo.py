# Obliczyć liczbę pi za pomocą algorytmu Monte Carlo.
# Wykorzystać losowanie punktów z kwadratu z wpisanym kołem.
# Sprawdzić zależność dokładności wyniku od liczby losowań. Wskazówka: Skorzystać z modułu random
import random


def calc_pi(n=100):
    """Obliczanie liczby pi metodą Monte Carlo.
    n oznacza liczbę losowanych punktów."""

    points_in_circle = 0

    for i in range(n):
        x = 2 * random.random() - 1
        y = 2 * random.random() - 1

        if x * x + y * y <= 1:
            points_in_circle += 1

    return points_in_circle / n * 4


if __name__ == "__main__":
    print(calc_pi())
    print(calc_pi(1000))
    print(calc_pi(10000))
    print(calc_pi(100000))
    print(calc_pi(1000000))

# Im więcej losowań tym wynik jest dokładniejszy
