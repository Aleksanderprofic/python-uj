# Zbadać problem szukania rozwiązań równania liniowego postaci a * x + b * y + c = 0. Podać specyfikację problemu.
# Podać algorytm rozwiązania w postaci listy kroków, schematu blokowego, drzewa. Podać implementację algorytmu
# w Pythonie w postaci funkcji solve1(), która rozwiązania wypisuje w formie komunikatów.

# Lista krokow:
# 1. Wczytanie a, b oraz c.
# 2. Czy a = 0?
#   Tak:
#       Czy b = 0?
#           Tak -> To nie jest równanie liniowe
#           Nie -> Równanie ma postać: b*y + c = 0, stąd y = -c/b
#   Nie:
#       Czy b = 0?
#           Tak -> Równanie ma postać: a*x + c = 0, stąd x = -c/a
#           Nie -> Stąd y = -a/b*x - c/b, gdzie pierwiastkami są dowolne pary liczb (-c/a,r),
#           gdzie r wybieramy dowolnie.
#
# Schemat blokowy:
#                         start
#                           |
#                           |
#                    Wczytanie a, b, c
#                           |
#                           |
#                ________ a == 0 ________
#            tak|                        |nie
#               |                        |
#      ______ b == 0 _____      _____ b == 0 _____
#  tak|                nie|    |tak            nie|
#     |                   |    |                  |
# To nie jest       y = -c/b   |           y = -(a*x + c)/b
# równanie                     |
# liniowe                   x = -c/a
#     |
#    Stop
#
# Drzewo:
#                               a == 0
#                                 /\
#                            tak /  \ nie
#                               /    \
#                              /      \
#                             /        \
#                            /          \
#                           /            \
#                       b == 0           b == 0
#                        /\                 /\
#                   tak /  \ nie       tak /  \ nie
#                      /    \             /    \
#                     /    y = -c/b      /      \
#                    /                  /        \
#                   /                  /          \
#             To nie jest       x = -c/a    y = -(a*x + c)/b
#               równanie
#               liniowe


def solve1(a, b, c):
    """Rozwiązywanie równania liniowego a x + b y + c = 0."""
    if not all(isinstance(coefficient, (int, float)) for coefficient in (a, b, c)):
        raise ValueError("Wrong arguments")
    if a == 0:
        if b == 0:
            print("This is not a linear equation because coefficient b is equal to 0")
        else:
            print(f"y = -c/b -> y = -({c}/{b}) -> y = {-c/b}")
    else:
        if b == 0:
            print(f"x = -c/a -> x = -({c}/{a}) -> x = {-c/a}")
        else:
            print(f"y = -(a*x + c)/b -> y = -({a}*x + {c})/{b}")
            print("Rozwiazaniem jest kazda para liczb: (-c/a, r), gdzie r wybieramy dowolnie.\n"
                  f"({-c/a}, r)")
