# Za pomocą techniki programowania dynamicznego napisać program obliczający wartości funkcji P(i, j).
# Porównać z wersją rekurencyjną programu. Wskazówka: Wykorzystać tablicę dwuwymiarową (np. słownik)
# do przechowywania wartości funkcji. Wartości w tablicy wypełniać kolejno wierszami.

# P(0, 0) = 0.5,
# P(i, 0) = 0.0 dla i > 0,
# P(0, j) = 1.0 dla j > 0,
# P(i, j) = 0.5 * (P(i-1, j) + P(i, j-1)) dla i > 0, j > 0.
values = {}


def p(i, j):
    global values
    if i < 0 or j < 0:
        raise ValueError("Arguments have to be greater or equal to 0")
    if i == 0 or j == 0:
        if i > 0:
            return 0.0
        elif j > 0:
            return 1.0
        return 0.5

    if (i - 1, j) not in values:
        first = p(i - 1, j)
    else:
        first = values[(i - 1, j)]
    if (i, j - 1) not in values:
        second = p(i, j - 1)
    else:
        second = values[(i, j - 1)]

    result = 0.5 * (first + second)
    values[(i, j)] = result
    return result


if __name__ == "__main__":
    assert p(3, 2) == 0.3125
    assert p(5, 5) == 0.5
    assert p(0, 0) == 0.5
    assert p(1, 0) == 0.0
    assert p(0, 1) == 1.0
    assert p(1, 7) == 0.9921875
    assert p(7, 5) == 0.2744140625
