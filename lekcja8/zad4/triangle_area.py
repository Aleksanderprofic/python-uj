# Zaimplementować algorytm obliczający pole powierzchni trójkąta, jeżeli dane są
# trzy liczby będące długościami jego boków. Jeżeli podane liczby nie spełniają warunku trójkąta,
# to program ma generować wyjątek ValueError.
import numpy as np


def heron(a, b, c):
    """Obliczanie pola powierzchni trójkąta za pomocą wzoru
    Herona. Długości boków trójkąta wynoszą a, b, c."""
    if a + b <= c or a + c <= b or b + c <= a:
        raise ValueError("Sides of the triangle must meet the condition of the triangle: \n"
                         "a + b > c\n"
                         "a + c > b\n"
                         "b + c > a\n")

    p = 0.5 * (a + b + c)

    return np.sqrt(p * (p - a) * (p - b) * (p - c))


if __name__ == "__main__":
    assert heron(3, 4, 5) == 6.0
    assert heron(6, 4, 9) == 9.56229574945264
    assert heron(11, 2, 12) == 9.921567416492215
    assert heron(1, 2, 2) == 0.9682458365518543
    assert heron(5, 44, 40) == 62.88829382325458

