# Napisać wersję rekurencyjną wyszukiwania binarnego.


def binarne_rek(l, left, right, y):
    """Wyszukiwanie binarne w wersji rekurencyjnej."""
    try:
        if left > right:
            return None
        middle = (left + right)//2
        middle_val = l[middle]
        if y == middle_val:
            return middle
        elif y < middle_val:
            return binarne_rek(l, left, middle - 1, y)
        else:
            return binarne_rek(l, middle + 1, right, y)
    except IndexError as error:
        print(f"{error.args[0]}. Please adjust function parameters.")


if __name__ == "__main__":
    assert binarne_rek([1, 2, 3, 4, 5, 6, 7], 0, 6, 3) == 2
    assert binarne_rek([1, 13, 42, 199, 922, 19292, 34122, 999999], 0, 7, 8) is None
    assert binarne_rek([1, 13, 42, 199, 922, 19292, 34122, 999999], 0, 7, 922) == 4
