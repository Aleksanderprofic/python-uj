# Napisać program, który na listę L wstawi n liczb wylosowanych z zakresu od 0 do k-1.
# Następnie program wylosuje liczbę y z tego samego zakresu i znajdzie wszystkie jej wystąpienia na liście L
# przy pomocy wyszukiwania liniowego. [n=100, k=10]
import random


def find_all_occurrences(n=100, k=10):
    l = []

    for i in range(n):
        l.append(random.randint(0, k - 1))

    print(f"Randomly generated list: {l}")

    occurrences = []
    y = random.randint(0, k - 1)
    print(f"y: {y}")
    for i, number in enumerate(l):
        if number == y:
            occurrences.append(i)

    return occurrences


if __name__ == "__main__":
    print(f"Occurrences: {find_all_occurrences()}")
