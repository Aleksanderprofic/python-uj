import math
import numpy as np
import unittest

# zad1.2 Stworzyć plik fracs.py i zapisać w nim funkcje do działań na ułamkach.
# Ułamek będzie reprezentowany przez listę dwóch liczb całkowitych [licznik, mianownik].
# Napisać kod testujący moduł fracs. Nie należy korzystać z klasy Fraction z modułu fractions.
# Można wykorzystać funkcję fractions.gcd() implementującą algorytm Euklidesa.


def check_fracs(frac1, frac2):
    check_frac(frac1)
    check_frac(frac2)


def check_frac(frac):
    if not isinstance(frac, (list, tuple)):
        raise ValueError(str(frac) + " has to be a list")

    if len(frac) != 2:
        raise ValueError(str(frac) + " has to have 2 elements")

    if frac[1] == 0:
        raise ValueError("Denominator cannot be equal to zero")


def move_minus_sign_to_nominator(frac):
    if frac[1] < 0:
        frac[0] = -frac[0]
        frac[1] = -frac[1]


def fraction_shortening(frac):
    gcd = math.gcd(frac[0], frac[1])

    if gcd == 1:
        return frac

    return [frac[0] / gcd, frac[1] / gcd]


def cmp(a, b):
    return int(a < b) - int(a > b)


def add_frac(frac1, frac2):
    check_fracs(frac1, frac2)
    move_minus_sign_to_nominator(frac1)
    move_minus_sign_to_nominator(frac2)

    if frac1[1] == frac2[1]:
        added_frac = [frac1[0] + frac2[0], frac1[1]]
    else:
        lcm = np.lcm(frac1[1], frac2[1])
        added_frac = [frac1[0] * lcm // frac1[1] + frac2[0] * lcm // frac2[1], lcm]

    return fraction_shortening(added_frac)


def sub_frac(frac1, frac2):
    check_fracs(frac1, frac2)
    move_minus_sign_to_nominator(frac1)
    move_minus_sign_to_nominator(frac2)

    if frac1[1] == frac2[1]:
        subtracted_frac = [frac1[0] - frac2[0], frac1[1]]
    else:
        lcm = np.lcm(frac1[1], frac2[1])
        subtracted_frac = [frac1[0] * lcm // frac1[1] - frac2[0] * lcm // frac2[1], lcm]

    return fraction_shortening(subtracted_frac)


def mul_frac(frac1, frac2):
    check_fracs(frac1, frac2)
    move_minus_sign_to_nominator(frac1)
    move_minus_sign_to_nominator(frac2)

    multiplied_frac = [frac1[0] * frac2[0], frac1[1] * frac2[1]]

    return fraction_shortening(multiplied_frac)


def div_frac(frac1, frac2):
    inverted_frac = [frac2[1], frac2[0]]
    return mul_frac(frac1, inverted_frac)


def is_positive(frac):
    check_frac(frac)

    if (frac[0] > 0 and frac[1] > 0) or (frac[0] < 0 and frac[1] < 0):
        return True
    else:
        return False


def is_zero(frac):
    check_frac(frac)
    if frac[0] == 0:
        return True
    else:
        return False


def cmp_frac(frac1, frac2):
    check_fracs(frac1, frac2)
    move_minus_sign_to_nominator(frac1)
    move_minus_sign_to_nominator(frac2)

    if frac1[1] == frac2[1]:
        return cmp(frac1[0], frac2[0])
    else:
        lcm = np.lcm(frac1[1], frac2[1])
        return cmp(frac1[0] * lcm // frac1[1], frac2[0] * lcm // frac2[1])


def frac2float(frac):
    check_frac(frac)

    return frac[0] / frac[1]


class TestFractions(unittest.TestCase):

    def setUp(self):
        self.zero = [0, 1]

    def test_add_frac(self):
        self.assertEqual(add_frac([1, 2], [1, 3]), [5, 6])
        self.assertEqual(add_frac([-1, 2], [1, 3]), [-1, 6])
        self.assertEqual(add_frac([1, 2], [-1, 3]), [1, 6])
        self.assertEqual(add_frac([-1, 2], [-1, 3]), [-5, 6])
        self.assertEqual(add_frac([1, -2], [1, 3]), [-1, 6])
        self.assertEqual(add_frac([1, 2], [1, -3]), [1, 6])
        self.assertEqual(add_frac([1, -2], [1, -3]), [-5, 6])
        self.assertEqual(add_frac([1, 2], [1, 2]), [1, 1])
        self.assertEqual(add_frac([1, 2], [1, -2]), [0, 1])

    def test_sub_frac(self):
        self.assertEqual(sub_frac([1, 2], [1, 3]), [1, 6])
        self.assertEqual(sub_frac([-1, 2], [1, 3]), [-5, 6])
        self.assertEqual(sub_frac([1, 2], [-1, 3]), [5, 6])
        self.assertEqual(sub_frac([-1, 2], [-1, 3]), [-1, 6])
        self.assertEqual(sub_frac([1, -2], [1, 3]), [-5, 6])
        self.assertEqual(sub_frac([1, 2], [1, -3]), [5, 6])
        self.assertEqual(sub_frac([1, -2], [1, -3]), [-1, 6])
        self.assertEqual(sub_frac([1, 2], [1, 2]), [0, 1])
        self.assertEqual(sub_frac([1, 2], [1, -2]), [1, 1])

    def test_mul_frac(self):
        self.assertEqual(mul_frac([3, 4], [4, 7]), [3, 7])
        self.assertEqual(mul_frac([3, 4], [-4, 7]), [-3, 7])

    def test_div_frac(self):
        self.assertEqual(div_frac([3, 4], [4, 7]), [21, 16])

    def test_is_positive(self):
        self.assertTrue(is_positive([1, 2]))
        self.assertTrue(is_positive([-2, -6]))
        self.assertFalse(is_positive([3, -2]))
        self.assertFalse(is_positive([-2, 2]))

    def test_is_zero(self):
        self.assertTrue(is_zero([0, 2]))
        self.assertTrue(is_zero([0, -6]))
        self.assertFalse(is_zero([3, 2]))
        self.assertFalse(is_zero([-2, 2]))

    def test_cmp_frac(self):
        self.assertEqual(cmp_frac([1, 2], [1, 3]), -1)

    def test_frac2float(self):
        self.assertAlmostEqual(0.5, frac2float([1, 2]))
        self.assertEqual(0, frac2float([0, 2]))

    def test_check_frac(self):
        with self.assertRaises(ValueError) as context:
            check_frac([1, 0])

        self.assertTrue("Denominator cannot be equal to zero" in str(context.exception))

    def tearDown(self): pass


if __name__ == '__main__':
    unittest.main()
