# zad1.1 Stworzyć plik rekurencja.py i zapisać w nim funkcje z zadań 4.3 (factorial), 4.4 (fibonacci).
# Sprawdzić operacje importu i przeładowania modułu.

from zad1 import rekurencja, rekurencja as rek

print (rekurencja.factorial(6))
print (rekurencja.fibonacci(5))

print (rek.factorial(6))
print (rek.fibonacci(5))


from zad1.rekurencja import *

print (factorial(6))
print (fibonacci(5))


from zad1.rekurencja import factorial

print (factorial(6))


from zad1.rekurencja import fibonacci as fib

print (fib(5))

