# 4.3 Napisać iteracyjną wersję funkcji factorial(n) obliczającej silnię.


def factorial(n: int) -> int:
    if n < 0:
        raise ValueError("Argument 'n' has to be greater or equal to 0")
    if n == 1 or n == 0:
        return 1
    result = 1
    for i in range(2, n+1):
        result *= i
    return result


# 4.4 Napisać iteracyjną wersję funkcji fibonacci(n) obliczającej n-ty wyraz ciągu Fibonacciego.

def fibonacci(n: int) -> int:
    if n < 0:
        raise ValueError("Argument 'n' has to be greater or equal to 0")
    if n == 0:
        return 0

    fib_previous = 0
    fib_next = 1
    for i in range(2, n+1):
        fib_temp = fib_previous + fib_next
        fib_previous = fib_next
        fib_next = fib_temp

    return fib_next
