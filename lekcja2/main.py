# 2.9

file_to_copy = open("file_to_copy.txt", 'r')
file_to_save = open("file_to_save.txt", 'w')

for line in file_to_copy:
    if line[0] != '#':
        file_to_save.write(line)

file_to_copy.close()
file_to_save.close()

# 2.10
line = """multiline string
to perform
some very interesting action"""

words = line.split()
number_of_words_in_line = len(words)
print("Number of words in line: " + str(number_of_words_in_line))

# 2.11
print("\nZad 2.11")
word1 = "someword"
word2 = "s"

def print_word_with_underscores(word: str):
    assert len(word) > 0
    print(word[0], end="")
    for c in word[1:]:
        print("_" + c, end="")

print("Underlined %s: " % word1)
print_word_with_underscores(word1)
print("\n\nUnderlined %s: " % word2)
print_word_with_underscores(word2)
# Assertion error
# print("\n\nUnderlined empty word: ")
# print_word_with_underscores("")

# 2.12
print("\nZad 2.12")
def create_word_from_first_chars_of_line(line: str):
    chars_list = []
    for word in line.split():
        chars_list.append(word[0])

    return ''.join(chars_list)

def create_word_from_last_chars_of_line(line: str):
    chars_list = []
    for word in line.split():
        chars_list.append(word[-1])

    return ''.join(chars_list)

print("Word from first chars of line: " + create_word_from_first_chars_of_line(line))
print("Word from first chars of empty line: " + create_word_from_first_chars_of_line(""))
print("Word from last chars of line: " + create_word_from_last_chars_of_line(line))
print("Word from last chars of empty line: " + create_word_from_last_chars_of_line(""))

# 2.13
print("\nZad 2.13")

def sum_of_words_length(line: str):
    the_sum = 0
    for word in line.split():
        the_sum += len(word)

    return the_sum

print("Sum of the words length from line: %s" % sum_of_words_length(line))

# 2.14
print("\nZad 2.14")
def find_longest_word_and_its_length(line: str):
    longest_word = None
    length = 0
    for word in line.split():
        word_len = len(word)
        if(length < word_len):
            length = word_len
            longest_word = word

    return (longest_word, length)


print("Longest word in line and it's length: %s %d" % find_longest_word_and_its_length(line))

# 2.15
print("\nZad 2.15")

L = [1,5,82,24,55,1935,8281]

def create_word(L: list):
    return ''.join(map(str, L))

print("Word from list with numbers: %s" % create_word(L))

# 2.16
print("\nZad 2.16")

line = """multiline stringGvR
to perform
some very interesting action"""

print(line.replace("GvR", "Guido van Rossum"))

# 2.17
print("\nZad 2.17")

print("Sorted alphabetically: %s" % sorted(line.split()))
print("Sorted by length of word: %s" % sorted(line.split(), key=len))

# 2.18
print("\nZad 2.18")
big_integer = 2901928815303203294030230412012

def number_of_zeros_in_big_integer(big_integer: int):
    return len(list(filter(lambda c: c =='0', str(big_integer))))


print("Number of zeros in big integer: %d" % number_of_zeros_in_big_integer(big_integer))

# 2.19
print("\nZad 2.19")

L = [1, 15, 92, 992, 102, 6, 24, 57, 881, 321]

def create_sentence_from_three_digits_numbers(L: list):
    return ' '.join(map(lambda x: str(x).zfill(3), L))


print("Sentence from three digits numbers: %s" % create_sentence_from_three_digits_numbers(L))